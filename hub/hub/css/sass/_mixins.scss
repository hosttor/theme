@mixin clearfix {
  &:before, &:after {
    content: '';
    display: table;
  }
  &:after {
    clear: both;
  }
}

@mixin button {
  border: 0 none;
  text-decoration: none;
  outline: 0;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #ededed;
  font-size: 14px;
  font-weight: 700;
  color: $body-color;
  padding: 15px 31px;
  text-align: center;
  border: 2px solid #ededed;
  &:hover {
    background-color: $color;
    border-color: $color;
    opacity: 0.8;
    text-decoration: none;
    outline: 0;
  }
  &.button-primary,
  &.button--primary,
  &.btn-primary {
    background-color: $color;
    border-color: $color;
  }
  &.white {
    color: #FFF;
    border-color: #FFF;
    &:before {
      color: #FFF !important;
    }
    &:hover {
      color: #FFF;
      &:before {
        color: #FFF;
      }
    }
  }
}

@mixin fa {
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  font-size: inherit;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

@mixin field_social_icon($fa) {
  @include clearfix;
  .field__label {
    font-size: 16px;
  }
  a {
    font-size: 0;
    overflow: hidden;
    text-decoration: none;
    outline: 0;
    border: 0 none;
    &:hover, &:active, &:focus {
      text-decoration: none;
      outline: 0;
      border: 0 none;
      &:before {
        color: $color;
      }
    }
    &:before {
      @include fa;
      content: $fa;
      font-size: 16px;
      color: $body-color;

    }
  }
}

@mixin stroke_gap_icon {
  font-family: 'Stroke-Gap-Icons';
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;

  /* Better Font Rendering =========== */
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

@mixin font-open-sans {
  font-family: 'Open Sans', sans-serif;
}

@mixin font-montserrat {
  font-family: 'Montserrat', sans-serif;
}

@mixin font-oswald {
  font-family: 'Oswald', sans-serif;
}