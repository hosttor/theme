/**
 * Created by toan on 2/16/16.
 */
(function ($, Drupal, drupalSettings) {


    var baseUrl = drupalSettings.path.baseUrl;
    var langcode = drupalSettings.path.currentLanguage;

    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });


    $(document).ready(function () {
        windowScroll();

        domready(function () {
            // load images from instagram later.
            instagram_images_load();
        })

        // check main menu active items if has mutiple
        var $items = $('#main-menu ul.menu li a.is-active');
        $items.each(function (index, value) {
            var $item = $(this);
            if (index !== 0) {
                $item.removeClass('is-active');
            }
            $item.click(function () {
                $(this).addClass('is-active');
                $('#main-menu ul.menu li a').removeClass('is-active');
            });

        });

        // slider full height
        $(window).resize(function () {
            init_main_menu();
            sidebar_toggle();


            $layout = $(window);
            $menu_height = $('#header').outerHeight();
            var $top_bar_height = 0;
            if ($('#top-bar').length) {
                $top_bar_height = $('#top-bar').height();
            }

            $('.slide-inner').css({
                minHeight: $layout.height() - $menu_height - $top_bar_height
            });


        });
        $(window).resize();


        $('a[href*="#"]:not([href="#"])'
        ).on('click', function (e) {


            if (location.pathname.replace(baseUrl, '').replace(langcode, '').replace(/^\//, '') == this.pathname.replace(baseUrl, '').replace(langcode, '').replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                var $hash = this.hash;
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                var scrollToPosition = target.offset().top;
                if ($('body.header-fixed #header').length) {
                    scrollToPosition = scrollToPosition - $('body.header-fixed #header').outerHeight();
                }
                if (target.length) {

                    $('html, body').animate({
                        scrollTop: scrollToPosition
                    }, 600, 'swing');
                    e.preventDefault();
                }
            }
        });


    });

    Drupal.behaviors.hub = {
        attach: function (context, settings) {
            // main menu
            init_main_menu(context);
            sidebar_toggle();
            // Label to placeholder
            $("form.contact-form :input, form.user-login-form :input, form.user-register-form :input, form.user-pass :input, form.comment-form :input, .search-page-form :input, #views-exposed-form-blog-blog-page :input, #toggle-sidebar .block-search :input, .block-simplenews :input, #toggle-sidebar :input").not(':checkbox, :radio').each(function (index, elem) {
                var eId = $(elem).attr("id");
                var label = null;
                if (eId && (label = $(elem).parents("form").find("label[for=" + eId + "]")).length == 1) {
                    $(elem).attr("placeholder", $(label).html());
                    $(label).remove();
                }
            });

            // Search text field placeholder
            $('.sidebar input.form-search').attr('placeholder', Drupal.t('keyword ...'));

            $('.block-search .form-actions, .block-views-exposed-filter-blockblog-blog-page .form-actions').click(function () {

                $(this).parents('form').submit();
            });

            // skill bar
            skills_animation(context);

            // instagram carousel


            var $instagram = $('#section-instagram .block-instagram-block .group', context);

            if ($instagram.length) {
                var $instagram_container = $instagram.wrapAll('<div class="owl-carousel"></div>');
                $('#section-instagram .owl-carousel', context).owlCarousel({
                        'itemElement': '.group',
                        'pagination': false,
                        'responsive': true,
                        'paginationNumbers': false,
                        'rewindNav': true,
                        'items': 8
                    }
                );
            }

            // Portfolio

            $('.portfolio-grid', context).find('.colorbox').each(function () {

                var $this = $(this);
                var $portfolio_item = $this.parents('.portfolio-item');
                var $image_view = $portfolio_item.find('a.fa-eye');
                $image_view.addClass('colorbox cboxElement');
                $image_view.attr('href', $this.attr('href'));

            });


            // center portfolio entries.
            $('.portfolio-items', context).find('.portfolio-item').each(function () {
                var $current_portfolio_item = $(this);
                var $center_outter_div = $current_portfolio_item.find('.views-field-nothing');
                var $center_inner_div = $center_outter_div.find('.field-content');

                $center_inner_div.css({
                    marginTop: (($center_outter_div.height() - $center_inner_div.height()) / 2)
                });
            });


            // portfolio items click and mansonry

            domready(function () {
                // load images from instagram later.
                var $portfolio = $('.portfolio-items');
                if ($portfolio.length && $portfolio.hasClass('masonry-layout')) {
                    $portfolio.masonry('reloadItems');
                    $portfolio.masonry();
                }
            });

            

        }
    };


    // main menu active items on scroll
    var $main_menu_items = $('#main-menu ul.menu li a');
    var $window = $(window);

    function menu_check_if_in_view() {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);
        if ($('#top-bar').length) {
            if ($('#top-bar').length < window_top_position) {
                $('#toggle-sidebar .overlay, #toggle-sidebar').css('top', 0);
            } else {
                var $body_padding = parseInt($('body').css('padding-top'));
                $top_bar = $('#top-bar').height();
                $('#toggle-sidebar .overlay, #toggle-sidebar').css('top', $body_padding + $top_bar);

            }
        }

        $.each($main_menu_items, function () {
            var $currLink = $(this);
            var refElement = $('#' + $currLink.attr("href").replace(baseUrl, '').replace(langcode, '').replace('#', '').replace(/^\//, ''));
            if (refElement.length) {
                var $element = refElement;
                var element_height = $element.outerHeight();
                var element_top_position = $element.offset().top;
                var element_bottom_position = (element_top_position + element_height);


                //check to see if this current container is within viewport
                if ((element_bottom_position >= window_top_position ) &&
                    (element_top_position <= window_bottom_position)) {
                    $main_menu_items.removeClass('is-active');
                    $currLink.addClass('is-active');
                } else {
                    $currLink.removeClass('is-active');

                }
            }

        });
    }

    $window.on('scroll resize', menu_check_if_in_view);
    $window.trigger('scroll');


    function skills_animation(context) {
        $('ul.skills li', context).each(function () {
            var $this = $(this);
            var $percent = $this.attr('data-percent');
            $this.find('span').css({opacity: 0});
            $this.find('p').animate({
                width: $percent,
            }, 9000, function () {
                $this.find('span').css({opacity: 1}).text($percent);
            });
        });
    }

    function init_main_menu(context) {

        if ($('body.header-fixed #header').length) {
            var $body_padding = $('body').css('padding-top');
            var $header = $('#header');
            // $('#header', context).css({'top': $body_padding});
            $('body.header-fixed .layout-container', context).css('padding-top', $header.outerHeight());
        }

        // check if sub menu has active item then add active class to parent
        $('ul#main-navigation > li').each(function () {
            var $parent_item = $(this);
            var $active_item = $parent_item.find('ul.sub-menu a.is-active');
            if ($active_item.length) {
                $parent_item.children('a').addClass('is-active');
            }
        });

    }

    function windowScroll() {
        if (window.location.hash) {
            // Fragment exists
            var $location_target = $(window.location.hash);
            var $scrollToPosition = $location_target.offset().top;
            if ($('body.header-fixed #header').length) {
                $scrollToPosition = $scrollToPosition - $('body.header-fixed #header').outerHeight();
            }
            if ($location_target.length) {
                $('html, body').animate({
                    scrollTop: $scrollToPosition
                }, 900, 'swing', function () {

                });
            }
        }
    }

    function sidebar_toggle() {

        var $sidebar = $('#toggle-sidebar');
        var $open_btn = $('#sidebar-toggle-btn');

        var $top_bar = 0;
        var $body_padding = parseInt($('body').css('padding-top'));
        if ($('#top-bar').length) {
            $top_bar = $('#top-bar').height();
        }

        $('#toggle-sidebar .overlay, #toggle-sidebar').css('top', $body_padding + $top_bar);

        $('#sidebar-toggle-btn').on('click', function () {

            $body_padding = parseInt($('body').css('padding-top'));
            if ($('#top-bar').length) {
                $top_bar = $('#top-bar').height();
            }
            $('#toggle-sidebar .overlay, #toggle-sidebar').css('top', $body_padding + $top_bar);
            $sidebar.removeClass('animated slideOutRight').animateCss('slideInRight');
            $open_btn.animateCss('slideOutRight');
            $sidebar.find('.overlay').removeClass('animated fadeOut').animateCss('fadeIn');
            $sidebar.fadeIn(800, function () {
                var $this = $(this);
                $open_btn.addClass('disabled');
                $this.addClass('open').removeClass('slideInRight');
                $this.find('.form-text:first').focus();

            });


        });
        $('#sidebar-close-btn').on('click', function (e) {

            $sidebar.removeClass('animated slideInRight').animateCss('slideOutRight');
            $open_btn.removeClass('slideOutRight').animateCss('slideInRight');
            $sidebar.find('.overlay').removeClass('animated fadeIn').animateCss('fadeOut');
            $sidebar.fadeOut(800, function () {
                $(this).addClass('closed');
                $open_btn.removeClass('disabled');
            });

            e.preventDefault();
        });
    }

    function instagram_images_load() {
        $('.instagram-item').each(function () {
            var $this = $(this);
            var $image = $("<img />").attr("src", $this.attr('data-image'));
            $this.html($image);
        });
    }


})(jQuery, Drupal, drupalSettings);
